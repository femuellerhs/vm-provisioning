import { Component, ViewChild, OnDestroy } from '@angular/core';
import { NgTerminal } from 'ng-terminal';
import { Subscription } from 'rxjs';
import { VmService } from '../vm.service';

@Component({
  selector: 'app-terminal',
  templateUrl: './terminal.component.html',
  styleUrls: ['./terminal.component.css']
})
export class TerminalComponent implements OnDestroy {
  @ViewChild('term', { static: true }) terminal: NgTerminal;

  public connected: boolean = false;

  private socket: WebSocket = undefined;
  private subscription: Subscription = undefined;
  private queue = [];

  private watchdogInterval = undefined;
  private intialNewlineSent = false;

  constructor(private vmService: VmService) { }

  ngAfterViewInit(): void {
    this.connect();
  }

  ngOnDestroy(): void {
    if(this.watchdogInterval != undefined) {
      clearInterval(this.watchdogInterval);
    }
    
    if(this.socket != undefined) {
      this.socket.close();
    }
  }

  private async connect() {
    if(this.connected) {
      return;
    }
    
    if(this.socket != undefined) {
      this.socket.onopen = null;
      this.socket.onmessage = null;
      this.socket.onerror = null;
      this.socket.onclose = null;
      this.socket.close();
    }

    let url = await this.vmService.getConsoleEndpoint();
    this.socket = new WebSocket(url);
    this.socket.onopen = this.onOpen.bind(this);
    this.socket.onmessage = this.onMessage.bind(this);
    this.socket.onerror = this.onError.bind(this);
    this.socket.onclose = this.onClosed.bind(this);

    if(this.subscription != undefined) {
      this.subscription.unsubscribe();
    }
    this.subscription = this.terminal.keyEventInput.subscribe((input) => {
      const msg = this.str2ab(input.key);
      this.socket.send(msg);
    });
    this.terminal.setCols(80);
    this.terminal.setRows(24);
    this.updateTerminal();
    if(this.watchdogInterval == undefined) {
      this.watchdogInterval = setInterval(() => {
        if(!this.connected) {
          console.log("Console connect timed out. Trying again...")
          if(this.socket != undefined) {
            this.socket.close();
          }
          this.connect();
        }
      }, 5000);
    }
  }

  private async updateTerminal() {
    while(this.queue.length > 0) {
      let text = await this.queue.shift().text();
      this.terminal.write(text);
    }

    setTimeout(this.updateTerminal.bind(this), 5);
  }

  private onOpen() {
    console.log("Connected to Console");

    /*const message = `4${ btoa(
      JSON.stringify({
        Width:  Math.floor(10),
        Height: Math.floor(40),
      })
    ) }`;
    console.log(message);
    this.socket.send(message);*/

    this.connected = true;

    if(!this.intialNewlineSent) {
      this.socket.send(this.str2ab('\n'));
      this.intialNewlineSent = true;
    }
  }

  private onMessage(event) {
    this.queue.push(event.data)
  }

  private onError() {
    console.log("Console Error");
    this.connected = false;
  }

  private onClosed() {
    console.log("Console Connection Closed");
    this.connected = false;
  }

  private str2ab(str) {
    const enc = new TextEncoder();

    return enc.encode(str);
  }
}
