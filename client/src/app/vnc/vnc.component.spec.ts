import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VNCComponent } from './vnc.component';

describe('VNCComponent', () => {
  let component: VNCComponent;
  let fixture: ComponentFixture<VNCComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VNCComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VNCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
