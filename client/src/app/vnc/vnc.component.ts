import { Component, OnDestroy } from '@angular/core';

import RFB from "../../../node_modules/@novnc/novnc/core/rfb.js";
import { VmService } from '../vm.service';

@Component({
  selector: 'app-vnc',
  templateUrl: './vnc.component.html',
  styleUrls: ['./vnc.component.css']
})
export class VNCComponent implements OnDestroy {
  private rfb: RFB = undefined;

  constructor(private vmService: VmService) { }

  ngAfterViewInit(): void {
    this.startClient();
  }

  ngOnDestroy(): void {
    if(this.rfb != undefined) {
      this.rfb.disconnect();
    }
  }

  private async startClient() {
    let url = await this.vmService.getVncEndpoint();
    
    this.rfb = new RFB(document.getElementById("screen"), url);
  }
}
