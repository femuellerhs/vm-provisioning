import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';

import { VmService } from './vm.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {  
  public state: String = "Starting";
  public interval = undefined;

  constructor(private vmService: VmService) {}

  ngOnInit(): void {
      this.interval = setInterval(() => this.update(this.vmService), 1000);
      this.update(this.vmService);
  }

  ngOnDestroy(): void {
      clearInterval(this.interval);
  }

  createVm() {
    this.vmService.create();
    this.state = "Starting";
  }

  deleteVm() {
    this.vmService.delete();
    this.state = "Stopped";
  }

  async update(vmService: VmService) {
    this.state = await vmService.getState();
  }
}
