import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VmService {
  constructor(private http: HttpClient) { }

  async getState() {
    let status = await this.getStatus();
    return status["state"];
  }

  async create() {
    await this.http.post("/vm", {}).toPromise();
  }

  async delete() {
    await this.http.delete("/vm").toPromise();
  }

  async getVncEndpoint(): Promise<string> {
    let status = await this.getStatus();

    if(status["state"] != "Running") {
      return "";
    }

    return this.getWebsocketUrl(status["vnc"]);
  }

  async getConsoleEndpoint(): Promise<string> {
    let status = await this.getStatus();

    if(status["state"] != "Running") {
      return "";
    }

    return this.getWebsocketUrl(status["console"]);
  }

  private async getStatus() {
    return await this.http.get("/vm", {}).toPromise();
  }

  private getWebsocketUrl(path: String): string {
    const host = window.location.hostname;
    const port = window.location.port;
  
    let url = "ws";
    if (window.location.protocol === "https:") {
      url = "wss";
    } else {
      url = "ws";
    }
    url += "://" + host;
    if (port) {
      url += ":" + port;
    }
    url += path;

    return url
}
}
