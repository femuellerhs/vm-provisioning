import json
import pprint
import requests
import secrets
import sys
import time
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

vm_name = 'rest-test'
image_id = 'default/image-skk8b'
storage_class_name = 'longhorn-image-skk8b'
disk_name = vm_name + "-disk-0-" + secrets.token_hex(8)
secret_name = vm_name + "-" + secrets.token_hex(8)

cpu = 4
memory = "4Gi"


def create_vm_payload():
  return {
  "type": "kubevirt.io.virtualmachine",
  "metadata": {
    "namespace": "default",
    "annotations": {
      "harvesterhci.io/volumeClaimTemplates": "[{\"metadata\":{\"name\":\"" + disk_name + "\",\"annotations\":{\"harvesterhci.io/imageId\":\"" + image_id + "\"}},\"spec\":{\"accessModes\":[\"ReadWriteMany\"],\"resources\":{\"requests\":{\"storage\":\"20Gi\"}},\"volumeMode\":\"Block\",\"storageClassName\":\"" + storage_class_name + "\"}}]",
      "network.harvesterhci.io/ips": "[]",
      "field.cattle.io/description": "Check REST functionality"
    },
    "labels": {
      "harvesterhci.io/creator": "harvester",
      "harvesterhci.io/os": "linux"
    },
    "name": vm_name
  },
  "spec": {
    "running": True,
    "template": {
      "metadata": {
        "annotations": {
          "harvesterhci.io/sshNames": "[]"
        },
        "labels": {
          "harvesterhci.io/vmName": vm_name
        }
      },
      "spec": {
        "domain": {
          "machine": {
            "type": ""
          },
          "cpu": {
            "cores": cpu,
            "sockets": 1,
            "threads": 1
          },
          "devices": {
            "inputs": [
              {
                "bus": "usb",
                "name": "tablet",
                "type": "tablet"
              }
            ],
            "interfaces": [
              {
                "masquerade": {},
                "model": "virtio",
                "name": "default"
              }
            ],
            "disks": [
              {
                "name": "disk-0",
                "disk": {
                  "bus": "virtio"
                },
                "bootOrder": 1
              },
              {
                "name": "cloudinitdisk",
                "disk": {
                  "bus": "virtio"
                }
              }
            ]
          },
          "resources": {
            "limits": {
              "memory": memory,
              "cpu": cpu
            }
          }
        },
        "evictionStrategy": "LiveMigrate",
        "hostname": vm_name,
        "networks": [
          {
            "pod": {},
            "name": "default"
          }
        ],
        "volumes": [
          {
            "name": "disk-0",
            "persistentVolumeClaim": {
              "claimName": disk_name
            }
          },
          {
            "name": "cloudinitdisk",
            "cloudInitNoCloud": {
              "secretRef": {
                "name": secret_name
              },
              "networkDataSecretRef": {
                "name": secret_name
              }
            }
          }
        ]
      }
    }
  }
}


def create_secrets_payload(vm_uid: str):
  return {
    "metadata": {
      "name": secret_name,
      "namespace": "default",
      "labels": {
        "harvesterhci.io/cloud-init-template": "harvester"
      },
      "ownerReferences": [
        {
          "name": vm_name,
          "kind": "VirtualMachine",
          "uid": vm_uid,
          "apiVersion": "kubevirt.io/v1"
        }
      ]
    },
    "type": "secret",
    "data": {
      "userdata": "I2Nsb3VkLWNvbmZpZwpwYWNrYWdlX3VwZGF0ZTogdHJ1ZQpwYWNrYWdlczoKICAtIHFlbXUtZ3Vlc3QtYWdlbnQKcnVuY21kOgogIC0gLSBzeXN0ZW1jdGwKICAgIC0gZW5hYmxlCiAgICAtICctLW5vdycKICAgIC0gcWVtdS1ndWVzdC1hZ2VudApzc2hfYXV0aG9yaXplZF9rZXlzOiBbXQo=",
      "networkdata": None
    }
  }


def create(base_url: str, token: str):
  response = requests.post(
      f"{base_url}/v1/harvester/kubevirt.io.virtualmachines/default",
      headers = {
          'Authorization': token,
          'Content-Type': 'application/json',
      },
      data = json.dumps(create_vm_payload()),
      verify = False,
  )

  if response.status_code != 201:
    print("Failed to create VM! Status code:", response.status_code)
    sys.exit(1)

  print("Created VM")


  data = response.json()
  uid = data["metadata"]["uid"]

  response = requests.post(
      f"{base_url}/v1/harvester/secrets/default",
      headers = {
          'Authorization': token,
          'Content-Type': 'application/json',
      },
      data = json.dumps(create_secrets_payload(uid)),
      verify = False,
  )

  if response.status_code != 201:
    print("Failed to create secrets! Status code:", response.status_code)
    sys.exit(1)
  print("Created secrets")

  print("Waiting for VM to become ready...")
  start = time.time()
  state = ""
  while state != "Running":
    status = get_status(base_url, token)

    if status is None:
      print("Failed to retrieve status!")
      continue

    if status["state"] != state:
      print(f"State changed: {status['state']} ({round(time.time() - start, 2)}s elapsed)")
    state = status["state"]

    time.sleep(1)


def get_status(base_url: str, token: str):
  response = requests.get(
      f"{base_url}/v1/harvester/kubevirt.io.virtualmachines/default/" + vm_name,
      headers = {
          'Authorization': token,
      },
      verify = False,
  )

  if response.status_code != 200:
    return None

  data = response.json()
  volumes = data["spec"]["template"]["spec"]["volumes"]
  volumes = [v for v in volumes if v['name'] != 'cloudinitdisk']

  return {
    'state': data.get("status", {}).get("printableStatus", "Unknown"),
    'uid': data["metadata"]["uid"],
    'volumes': volumes
  }


def delete(base_url: str, token: str):
  status = get_status(base_url, token)
  if status is None:
    print("Cannot retrieve status!")
    sys.exit(1)
  volumes = status['volumes']

  response = requests.delete(
    f"{base_url}/v1/harvester/kubevirt.io.virtualmachines/default/{vm_name}",
     headers = {
        'Authorization': token,
    },
    verify = False,
  )

  if response.status_code != 200:
    print("Failed to delete VM! Status code:", response.status_code)
    sys.exit(1)
  print("Deleted VM")

  for volume in volumes:
    response = requests.delete(
      f"{base_url}/v1/harvester/persistentvolumeclaims/default/" + volume['persistentVolumeClaim']['claimName'],
      headers = {
          'Authorization': token,
      },
      verify = False,
    )

    if response.status_code != 200:
      print("Failed to delete disk! Status code:", response.status_code)
      sys.exit(1)
    print(f"Deleted disk {volume['name']} (Claim: {volume['persistentVolumeClaim']['claimName']})")


if __name__ == '__main__':
  usage = f'Usage: {sys.argv[0]} [Harvester Base URL] [Harvester API Bearer Token] create|status|delete\n\nExample: {sys.argv[0]} "https://127.0.0.1" "Bearer abcdef" create'

  if len(sys.argv) != 4:
    print(usage)
  elif sys.argv[3] == "create":
    print("===== Creating VM =====")
    create(sys.argv[1], sys.argv[2])
  elif sys.argv[3] == "status":
    print("===== Retrieving VM Status =====")
    pprint.pprint(get_status(sys.argv[1], sys.argv[2]))
  elif sys.argv[3] == "delete":
    print("===== Deleting VM =====")
    delete(sys.argv[1], sys.argv[2])
  else:
    print(usage)