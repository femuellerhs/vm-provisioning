# Automatic VM Provisioning with Harvester

This repository contains several scripts/apps to demonstrage automatic provisioning with the [Harvester](https://docs.harvesterhci.io/v1.0/) hypervisor.

Currently the following examples are available:
* Python client for simple REST-API interaction
* Web Application with *NestJS* as backend and *Angular* as frontend

Requirements (independent of example):
* A harvester installation
  * Base URL for harvester
  * API-Token (Click Profiel Icon at the top-right -> Account & API-Keys -> Create API Key -> Manually copy the Bearer token)
* Cloud Base Image in Harvester
  * See [here](https://docs.harvesterhci.io/v1.0/upload-image/) for instructions on how to add them
  * The easiest image would be the [Ubuntu Server Cloud Image](https://cloud-images.ubuntu.com/focal/current/) but that has no Desktop by default
  * You can also upload the harddrive file of a local VM
    * This file musst be in qcow2, raw or ISO format
    * You should probably install cloud-init inside the VM (otherwise for example networking may not work)
    * This Tutorial may be helpful: [Youtube](https://www.youtube.com/watch?v=xPkojylNPCQ)
  
## Simple Python Client
A python client can be used for simple interaction with the Harvester API.

Requirements:
* Install the following Python dependencies:
  * requests
  * pprint
* Set variables at the top of *python/vm.py* according to your Harvester instance

For usage information run:
```shell
python3 python/vm.py
```

## Web Application
A webapp consisting of NestJS (backend) and Angular (frontend).

Requirements:
* npm
* angular-cli 

## Running
Compile frontend inside the *client/* directory (only required once or when Angular code changed):
```shell
npm install # only needs to be run once (or when dependencies changed)
ng build
```
Modify the variables inside *[Repository Root]/src/app.service.ts* according to your Harvester installation.

Start backend inside the root directory of this repository:
```shell
npm install # only needs to be run once (or when dependencies changed)
npm run start:dev
```

This will start a webserver on [http://localhost:3000](http://localhost:3000) which will serve the compiled Angular application on `/` and proxies requests on `/apis` to Harvester (very unsafe, only for demonstration!).