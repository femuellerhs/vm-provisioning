import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { createProxyMiddleware } from 'http-proxy-middleware';
import { baseUrl, token } from './app.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.use(
    '/apis',
    createProxyMiddleware({
      target: baseUrl,
      ws: true,
      secure: false,
      headers: {
        'Authorization': token,
      },
    })
  );

  await app.listen(3000);
  console.log(`Application is running on: ${await app.getUrl()}`);
}
bootstrap();
