import { Controller, Get, Post, Delete } from '@nestjs/common';
import { AppService } from './app.service';

@Controller("/vm")
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async getState(): Promise<Object> {
    return await this.appService.getState();
  }

  @Post()
  create(): Object {
    this.appService.createVm();
    return {};
  }

  @Delete()
  delete(): Object {
    this.appService.deleteVm();
    return {};
  }
}
