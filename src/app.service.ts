import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import * as crypto from 'crypto';
import { firstValueFrom } from 'rxjs';

// Modify these according to your Harvester installation
let baseUrl = 'https://172.16.10.249'; // Harvester URL - Unsafe SSL certificates will be ignored
let token = 'Bearer token-gmn2r:lr5pcvnfmsr4dfg96qm49xkwkpbtpnmgqcwg5ktz488k5wrpgz88qt'; // Your generated API token
let imageId = 'default/image-tqrdb'; // Create a VM inside the Harvester GUI and look at the YAML to get this Id
let storageClassName = 'longhorn-image-tqrdb'; // Create a VM inside the Harvester GUI and look at the YAML to get this Id
////////////////////////////
export {baseUrl, token};

let vmName = 'rest-test';
let diskName = vmName + "-disk-0-" + crypto.randomBytes(8).toString("hex");
let secretName = vmName + "-" + crypto.randomBytes(8).toString("hex");

let cpu = 4;
let memory = "4Gi";

function createVmPayload() {
  return {
    "type": "kubevirt.io.virtualmachine",
    "metadata": {
      "namespace": "default",
      "annotations": {
        "harvesterhci.io/volumeClaimTemplates": "[{\"metadata\":{\"name\":\"" + diskName + "\",\"annotations\":{\"harvesterhci.io/imageId\":\"" + imageId + "\"}},\"spec\":{\"accessModes\":[\"ReadWriteMany\"],\"resources\":{\"requests\":{\"storage\":\"20Gi\"}},\"volumeMode\":\"Block\",\"storageClassName\":\"" + storageClassName + "\"}}]",
        "network.harvesterhci.io/ips": "[]",
        "field.cattle.io/description": "Check REST functionality"
      },
      "labels": {
        "harvesterhci.io/creator": "harvester",
        "harvesterhci.io/os": "linux"
      },
      "name": vmName
    },
    "spec": {
      "running": true,
      "template": {
        "metadata": {
          "annotations": {
            "harvesterhci.io/sshNames": "[]"
          },
          "labels": {
            "harvesterhci.io/vmName": vmName
          }
        },
        "spec": {
          "domain": {
            "machine": {
              "type": ""
            },
            "cpu": {
              "cores": cpu,
              "sockets": 1,
              "threads": 1
            },
            "devices": {
              "inputs": [
                {
                  "bus": "usb",
                  "name": "tablet",
                  "type": "tablet"
                }
              ],
              "interfaces": [
                {
                  "masquerade": {},
                  "model": "virtio",
                  "name": "default"
                }
              ],
              "disks": [
                {
                  "name": "disk-0",
                  "disk": {
                    "bus": "virtio"
                  },
                  "bootOrder": 1
                },
                {
                  "name": "cloudinitdisk",
                  "disk": {
                    "bus": "virtio"
                  }
                }
              ]
            },
            "resources": {
              "limits": {
                "memory": memory,
                "cpu": cpu
              }
            }
          },
          "evictionStrategy": "LiveMigrate",
          "hostname": vmName,
          "networks": [
            {
              "pod": {},
              "name": "default"
            }
          ],
          "volumes": [
            {
              "name": "disk-0",
              "persistentVolumeClaim": {
                "claimName": diskName
              }
            },
            {
              "name": "cloudinitdisk",
              "cloudInitNoCloud": {
                "secretRef": {
                  "name": secretName
                },
                "networkDataSecretRef": {
                  "name": secretName
                }
              }
            }
          ]
        }
      }
    }
  };
}

function createSecretsPayload(vmUid) {
  return {
    "metadata": {
      "name": secretName,
      "namespace": "default",
      "labels": {
        "harvesterhci.io/cloud-init-template": "harvester"
      },
      "ownerReferences": [
        {
          "name": vmName,
          "kind": "VirtualMachine",
          "uid": vmUid,
          "apiVersion": "kubevirt.io/v1"
        }
      ]
    },
    "type": "secret",
    "data": {
      "userdata": "I2Nsb3VkLWNvbmZpZwpwYWNrYWdlX3VwZGF0ZTogdHJ1ZQpwYWNrYWdlczoKICAtIHFlbXUtZ3Vlc3QtYWdlbnQKcnVuY21kOgogIC0gLSBzeXN0ZW1jdGwKICAgIC0gZW5hYmxlCiAgICAtICctLW5vdycKICAgIC0gcWVtdS1ndWVzdC1hZ2VudApzc2hfYXV0aG9yaXplZF9rZXlzOiBbXQo=",
      "networkdata": undefined
    }
  };
}

@Injectable()
export class AppService {
  constructor(private httpService: HttpService) {}

  async getState() {
    try {
      let status = await this.getStatus();
      if(status["state"] != "Running") {
        return { "state" : "Starting" };   
      }
      return { 
        "state" : "Running",
        "console": `/apis/subresources.kubevirt.io/v1/namespaces/default/virtualmachineinstances/${vmName}/console`,
        "vnc": `/apis/subresources.kubevirt.io/v1/namespaces/default/virtualmachineinstances/${vmName}/vnc`,
      }; 
    } catch(e) {
      return { "state" : "Stopped" }; 
    }
  }

  async createVm() {
    let response = await firstValueFrom(
      this.httpService.post(
        `${baseUrl}/v1/harvester/kubevirt.io.virtualmachines/default`,
        createVmPayload(),
        {
          headers: {
            'Authorization': token,
            'Content-Type': 'application/json'
          },
        }
      )
    );
    
    if(response.status != 201) {
      console.log("Failed to create VM! Status code: " + response.status);
      return false;
    }
    console.log("Created VM")

    let data = response.data;
    let uid = data["metadata"]["uid"];

    response = await firstValueFrom(
      this.httpService.post(
        `${baseUrl}/v1/harvester/secrets/default`,
        createSecretsPayload(uid),
        {
          headers: {
            'Authorization': token,
            'Content-Type': 'application/json'
          },
        }
      )
    );

    if(response.status != 201) {
      console.log("Failed to create secrets! Status code: " + response.status);
      return false;
    }
    console.log("Created secrets")

    let start = new Date().getTime() / 1000.0;
    let state = "";
    while(state != "Running") {
      let status = await this.getStatus();

      if(status == undefined) {
        console.log("Failed to retrieve status!");
        continue;
      }

      if(status["state"] != state) {
        let t = (new Date().getTime() / 1000.0) - start;
        console.log(`State changed: ${status["state"]} (${t}s elapsed)`);
      }
      state = status["state"];

      await new Promise(resolve => setTimeout(resolve, 1000));
    }
    console.log("VM is ready");

    return true;
  }

  async deleteVm() {
    let status = await this.getStatus();
    if(status == undefined) {
      console.log("Failed to retrieve status!");
      return false;
    }

    let volumes = status['volumes']

    let response = await firstValueFrom(
      this.httpService.delete(
        `${baseUrl}/v1/harvester/kubevirt.io.virtualmachines/default/${vmName}`,
        {
          headers: {
            'Authorization': token,
          },
        }
      )
    );

    if(response.status != 200) {
      console.log("Failed to delete VM! Status code: " + response.status);
      return false;
    }
    console.log("Deleted VM");

    for(let volume of volumes) {
      response = await firstValueFrom(
        this.httpService.delete(
          `${baseUrl}/v1/harvester/persistentvolumeclaims/default/` + volume['persistentVolumeClaim']['claimName'],
          {
            headers: {
              'Authorization': token,
            },
          }
        )
      );

      if(response.status != 200) {
        console.log("Failed to delete disk! Status code: " + response.status);
        return false;
      }
      console.log(`Deleted disk ${volume["name"]} (Claim: ${volume["persistentVolumeClaim"]["claimName"]})`);
    }

    return true;
  }

  private async getStatus() {
    let response = await firstValueFrom(
      this.httpService.get(
        `${baseUrl}/v1/harvester/kubevirt.io.virtualmachines/default/` + vmName,
        {
          headers: {
            'Authorization': token,
          },
        }
      )
    );

    if(response.status != 200) {
      return undefined;
    }

    let data = response.data;
    let volumes = data["spec"]["template"]["spec"]["volumes"];
    volumes = volumes.filter(volume => volume["name"] != "cloudinitdisk");

    let state = "Unknown";
    if("status" in data && "printableStatus" in data["status"]) {
      state = data["status"]["printableStatus"];
    }

    return {
      "state": state,
      "uid": data["metadata"]["uid"],
      "volumes": volumes
    };
  }
}
